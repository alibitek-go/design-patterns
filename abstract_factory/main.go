package main

import (
	"fmt"
	"reflect"
)

type (
	file struct {
		name string
		content string
	}

	ntfs struct {
		files map[string]file
	}

	ext4 struct {
		files map[string]file
	}

	FileSystem interface {
		CreateFile(string)
		FindFile(string) file
	}
)

func (fs ntfs) CreateFile(path string) {
	f := file{name: path, content: "NTFS file"}
	fs.files[path] = f
	fmt.Println("NTFS")
}

func (fs ext4) CreateFile(path string) {
	f := file{name: path, content: "EXT4 file"}
	fs.files[path] = f
	fmt.Println("EXT4")
}

func (fs ntfs) FindFile(path string) file {
	f, ok := fs.files[path]
	if !ok {
		return file{}
	}
	return f
}

func (fs ext4) FindFile(path string) file {
	f, ok := fs.files[path]
	if !ok {
		return file{}
	}
	return f
}

func FileSystemFactory(env string) interface{} {
	switch env {
	case "production":
		return ntfs{
			files: make(map[string]file),
		}
	case "development":
		return ext4{
			files: make(map[string]file),
		}
	default:
		return nil
	}
}

type Factory func(string) interface{}

func AbstractFactory(factory string) Factory {
	switch factory {
	case "filesystem":
		return FileSystemFactory
	default:
		return nil
	}
}

func SetupConstructors(env string) FileSystem {
	fs := AbstractFactory("filesystem")
	return fs(env).(FileSystem)
}

func main() {
	env1 := "production"
	env2 := "development"

	fs1 := SetupConstructors(env1)
	fs2 := SetupConstructors(env2)

	fs1.CreateFile("/root/ntfs.txt")
	fmt.Println(fs1.FindFile("/root/ntfs.txt"))

	fs2.CreateFile("/root/ext4.txt")
	fmt.Println(fs2.FindFile("/root/ext4.txt"))

	fmt.Println(reflect.TypeOf(fs1).Name())
	fmt.Println(reflect.TypeOf(&fs1).Elem())

	fmt.Println(reflect.TypeOf(fs2).Name())
	fmt.Println(reflect.TypeOf(&fs2).Elem())
}
