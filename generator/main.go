package main

import "fmt"

func fib(n int) chan int {
	out := make(chan int)

	// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
	go func() {
		defer close(out)
		for i, j := 0, 1; i < n; i, j = i+j, i {
			out <- i
		}
	}()

	return out
}
func main() {
	for x := range fib(10000000) {
		fmt.Println(x)
	}
}
