package main

import (
	"fmt"
	"reflect"
)

type (
	mongoDB struct {
		database map[string]string
	}

	sqlite struct {
		database map[string]string
	}

	Database interface {
		GetData(string) string
		PutData(string, string)
	}
)

func (mongo mongoDB) GetData(query string) string {
	data, ok := mongo.database[query]
	if !ok {
		return ""
	}

	fmt.Println("MongoDB")
	return data
}

func (sql sqlite) GetData(query string) string {
	data, ok := sql.database[query]
	if !ok {
		return ""
	}

	fmt.Println("SQLite")
	return data
}

func (mongo mongoDB) PutData(query, data string) {
	mongo.database[query] = data
}

func (sql sqlite) PutData(query, data string) {
	sql.database[query] = data
}

func DatabaseFactory(env string) Database {
	switch env {
	case "production":
		return mongoDB{
			database: make(map[string]string),
		}
	case "development":
		return sqlite{
			database: make(map[string]string),
		}
	default:
		return nil
	}
}

func main() {
	env1 := "production"
	env2 := "development"

	db1 := DatabaseFactory(env1)
	db2 := DatabaseFactory(env2)

	db1.PutData("test", "This is MongoDB!")
	fmt.Println(db1.GetData("test"))

	db2.PutData("test", "This is SQLite!")
	fmt.Println(db2.GetData("test"))

	fmt.Println(reflect.TypeOf(db1).Name())
	fmt.Println(reflect.TypeOf(&db1).Elem())

	fmt.Println(reflect.TypeOf(db2).Name())
	fmt.Println(reflect.TypeOf(&db2).Elem())
}
