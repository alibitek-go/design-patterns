package main

import (
	"fmt"
	"math"
	"sync"
	"time"
)

type (
	Event struct {
		data int
	}

	Observer interface {
		NotifyCallback(Event)
	}

	Subject interface {
		AddListener(Observer)
		RemoveListener(Observer)
		Notify(Event)
	}

	eventObserver struct {
		id int
		time time.Time // start time, i.e. the moment when the event started to be observed
	}

	eventSubject struct {
		observers sync.Map
	}
)

func (e *eventObserver) NotifyCallback(event Event) {
	fmt.Printf("[Observer #%d] Received: %d after %v\n", e.id, event.data, time.Since(e.time))
}

func (s *eventSubject) AddListener(observer Observer) {
	s.observers.Store(observer, struct{}{})
}

func (s *eventSubject) RemoveListener(observer Observer) {
	s.observers.Delete(observer)
}

func (s *eventSubject) Notify(event Event) {
	s.observers.Range(func(key interface{}, value interface{}) bool {
		if key == nil || value == nil {
			return false
		}
		key.(Observer).NotifyCallback(event)
		return true
	})
}

func main() {
	subject := eventSubject{
		observers: sync.Map{},
	}

	var observer1 = eventObserver{id: 1, time: time.Now()}
	var observer2 = eventObserver{id: 2, time: time.Now()}

	subject.AddListener(&observer1)
	subject.AddListener(&observer2)

	// Remove first observer after 5µs to test that only observer 2 received the notification
	go func() {
		select {
			case <- time.After(time.Microsecond * 5):
				subject.RemoveListener(&observer1)
		}
	}()

	for x := range [10]int{} {
		subject.Notify(Event{data: int(math.Pow(10, float64(x)))})
	}
}
