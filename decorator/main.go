package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"sync"
	"time"
)

type piFunc func (int) float64

func loggerMiddleware(fun piFunc, logger *log.Logger) piFunc {
	return func (n int) float64 {
		fn := func (n int) (result float64) {
			defer func(startTime time.Time) {
				logger.Printf("took=%v, n=%v, result=%v", time.Since(startTime), n, result)
			}(time.Now())
			return fun(n)
		}
		return fn(n)
	}
}

// logger(cache(Pi(n)))
func cacheMiddleware(fun piFunc, cache *sync.Map) piFunc {
	return func (n int) float64 {
		fn := func (n int) float64 {
			key := fmt.Sprintf("n=%d", n)
			if val, ok := cache.Load(key); ok {
				return val.(float64)
			}
			result := fun(n)
			cache.Store(key, result)
			return result
		}
		return fn(n)
	}
}

// PI approximation
func Pi(n int) float64 {
	ch := make(chan float64)

	for k := 0; k <= n; k++ {
		go func (ch chan float64, k float64) {
			ch <- 4 * math.Pow(-1, k) / (2 * k + 1)
		}(ch, float64(k))
	}

	result := 0.0
	for k := 0; k <= n; k++ {
		result += <-ch
	}

	return result
}

func divide(n int) float64 {
	return float64(n / 2)
}

func main() {
	fmt.Printf("%.9f\n%.9f\n", Pi(1000), Pi(50000))
	f := loggerMiddleware(Pi, log.New(os.Stdout, "logger decorator ", 1))
	f(100000)

	fs := cacheMiddleware(Pi, &sync.Map{})
	g := loggerMiddleware(fs, log.New(os.Stdout, "cache decorator ", 1))
	g(100000)
	g(20000)
	g(100000)

	h := cacheMiddleware(divide, &sync.Map{})
	i := loggerMiddleware(h, log.New(os.Stdout, "divide decorator ", 1))
	i(10000)
	i(2000)
	i(10)
	i(10000)
}
