# Golang design patterns
- [Decorator](decorator)
- [Generator](generator)
- [Observer](observer)
- [Factory](factory)
- [Abstract Factory](abstract_factory)
